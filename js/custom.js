// All custom js code goes here

//Bootstrap tooltip
$(function () {
  $('[data-toggle="tooltip"]').tooltip();
});

// Swiper - Testimonial
var mySwiper = new Swiper ('.reviews-container', {
  slidesPerView: 3,
  spaceBetween: 30,
  grabCursor: true,
  // If we need pagination
  pagination: {
    el: '.swiper-pagination',
    clickable: true
  },
  breakpoints: {
    1200: {
      slidesPerView: 3
    },
    992: {
      slidesPerView: 2
    },
    768: {
      slidesPerView: 1
    }
  
  }
});

// Swiper - Popular Trek
var mySwiper = new Swiper ('.trek-container', {
  slidesPerView: 4,
  spaceBetween: 30,
  grabCursor: true,
  // Navigation arrows
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },
  breakpoints: {
    1200: {
      slidesPerView: 4
    },
    992: {
      slidesPerView: 2
    },
    768: {
      slidesPerView: 1
    }
  
  }
});


// Form - Search (Home page)

$(document).ready(function(){
  var formElem = $('.form-control');
  formElem.each(function(){
    if($(this).val() != ""){
      $(this).parent().addClass('focused');
    }
  });
  $(formElem).on('focus', function(){
    $(this).parent().addClass('focused');
  })
  $(formElem).on('blur', function(){
    if($(this).val() == "") {
      $(this).parent().removeClass('focused');
    }
  });
});

// Sticky header
$(document).ready(function(){
  if ($(document).scrollTop() >= 150) {
    $('header').addClass('bg-primary shadow-sm');
  }
});
$(window).scroll(function(){
  if ($(document).scrollTop() >= 150) {
      $('header').addClass('bg-primary shadow-sm');
  }
  else {
      $('header').removeClass('bg-primary shadow-sm');
  }
});

// imagesLoaded
var imgLoad = imagesLoaded( 'img' );
imgLoad.on( 'fail', function( instance ) {
  console.log('FAIL - all images loaded, at least one is broken');
});

// Using for animation with animate.css
function animateCSS(element, animationName, callback) {
  const node = document.querySelector(element)
  node.classList.add('animated', animationName)

  function handleAnimationEnd() {
      node.classList.remove('animated', animationName)
      node.removeEventListener('animationend', handleAnimationEnd)

      if (typeof callback === 'function') callback()
  }

  node.addEventListener('animationend', handleAnimationEnd)
}

//animateCSS('.header-brand', 'bounce');
//animateCSS('.header .navbar-collapse', 'bounce');

$(document).ready(function(){
    setTimeout(function(){
      $('.header').css('transform', 'translateY(0)');
    }, 1000)
});